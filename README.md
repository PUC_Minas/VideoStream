# Servidor para streaming de videos

**Progresso:** CONLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2018<br />

### Objetivo
Implementar o servidor com as devidas requisições para streaming de midias, suportando o envio de legendas.

### Observação

IDE:  [Visual Studio 2017](https://visualstudio.microsoft.com/)<br />
Linguagem: [C#](https://dotnet.github.io/)<br />
Banco de dados: Não utiliza)<br />

### Execução

    $ dotnet restore
    $ dotnet run
    
### Pacotes

| Nome | Função |
| ------ | ------ |
| [TvDbSharper] | Biblioteca para recuperar dados sobre as séries |

[TvDbSharper]: <https://www.nuget.org/packages/TvDbSharper/>


### Contribuição

Esse projeto está finalizado é livre para o uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->