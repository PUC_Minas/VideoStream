﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VideoStream.Migrations
{
    public partial class IncludePosterOnEpisode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Poster",
                table: "Episodes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Poster",
                table: "Episodes");
        }
    }
}
