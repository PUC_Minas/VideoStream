﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VideoStream.Migrations
{
    public partial class IncludeTVDBIdOnSerie : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TheTVDBId",
                table: "Series",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TheTVDBId",
                table: "Series");
        }
    }
}
