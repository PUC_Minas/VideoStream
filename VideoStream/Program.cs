﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace VideoStream {
    public class Program {
        public static void Main(string[] args) {
            CreateWebHostBuilder( args ).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) {
            //WebHost.CreateDefaultBuilder( args )
            //    .UseStartup<Startup>();
            var lineCommandParams = new ConfigurationBuilder()
                .AddCommandLine( args )
                .Build(); // testar sem o build

            var hostUrl = lineCommandParams[ "ip" ] ?? "0.0.0.0";
            var hostPort = lineCommandParams[ "port" ] ?? "6001";

            var host = new WebHostBuilder()
                .UseKestrel( config => {
                    config.Limits.KeepAliveTimeout = TimeSpan.FromMinutes( 5 );
                    //config.Listen( IPAddress.Parse("0.0.0.0"), 6001 );
                    } )
                .UseUrls( "http://" + hostUrl + ":" + hostPort )
                .UseContentRoot( Directory.GetCurrentDirectory() )
                .UseStartup<Startup>();

            return host;
        }
    }

}
