﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TvDbSharper;
using TvDbSharper.Dto;

namespace VideoStream.Components {
    public class TheTVDBApi {
        private string _API_KEY = "C839E66449A3AB36";
        private string _USERNAME = "paulciclops";
        private string _USER_KEY = "E51B3E95BBDBF1BA";

        ITvDbClient client;

        public TheTVDBApi() {
            client = new TvDbClient();
        }

        public async Task Auth() {
            await client.Authentication.AuthenticateAsync( _API_KEY, _USERNAME, _USER_KEY );
        }

        public async Task<List<string>> GetImagesAsync(int serieId) {
            var resp = new List<string>();
            try {
                var imagesData = await client.Series.GetImagesAsync( serieId, new ImagesQuery { KeyType = KeyType.Poster } );
                foreach (var image in imagesData.Data) {
                    resp.Add( image.FileName );
                }
            } catch (Exception ex) {
                Console.WriteLine( "Error on get images: " + ex.Message );
            }
            return resp;
        }

        public async Task<EpisodeRecord> GetEpisodeImageAsync(int episodeId) {
            var resp = new EpisodeRecord();
            try {
                var imagesData = await client.Episodes.GetAsync( episodeId );
                resp = imagesData.Data;
            } catch (Exception ex) {
                Console.WriteLine( "Error on get images: " + ex.Message );
            }
            return resp;
        }

        public async Task<List<SeriesSearchResult>> SearchAsync(string serieName) {
            var response = await client.Search.SearchSeriesByNameAsync( serieName );
            var result = response.Data.ToList();
            return result;
        }

        public async Task<Series> GetSerie(int serieId) {
            var response = await client.Series.GetAsync( serieId );
            return response.Data;
        }

        public async Task<List<BasicEpisode>> GetSeasons(int serieId, int season) {
            List<BasicEpisode> resp = new List<BasicEpisode>();
            List<BasicEpisode> response = new List<BasicEpisode>();
            var page = 1;

            var totalEpisodes = Convert.ToInt32( ( await GetSeasonsSummary( serieId ) ).AiredEpisodes );

            do {
                response = ( await client.Series.GetEpisodesAsync( serieId, page ) ).Data.ToList();
                for (int i = 0; i < response.Count(); i++) {
                    if (response[ i ].AiredSeason == season) {
                        resp.Add( response[ i ] );
                    }
                }
                page++;
            }
            while (resp.Count() < totalEpisodes && response.Last().AiredSeason <= season);

            return resp;
        }

        public async Task<EpisodesSummary> GetSeasonsSummary(int serieId) {
            var response = await client.Series.GetEpisodesSummaryAsync( serieId );
            return response.Data;
        }
    }
}
