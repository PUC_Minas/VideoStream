﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoStream.Models {
    public class Serie {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Poster { get; set; }
        public int TheTVDBId { get; set; }
        public List<Season> Seasons { get; set; }
    }
}
