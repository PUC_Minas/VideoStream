﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoStream.Models {
    public class Season {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Poster { get; set; }

        public int SerieId { get; set; }

        public List<Episode> Episodes { get; set; }
    }
}
