﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoStream.Models {
    public class Episode {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Title { get; set; }
        public string Poster { get; set; }
        public string Description { get; set; }
        public string File { get; set; }

        public int SeasonId { get; set; }
    }
}
