﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoStream.Models {
    public class DatabaseContext: DbContext {
        public DatabaseContext(DbContextOptions<DatabaseContext> options): base(options) {
            Database.EnsureCreated();
        }

        public DbSet<Serie> Series { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<Episode> Episodes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder) {
            builder.Entity<Serie>()
                .HasKey( m => m.Id );

            builder.Entity<Serie>()
                .HasMany( m => m.Seasons );

            builder.Entity<Season>()
                .HasKey( m => m.Id );

            builder.Entity<Season>()
                .HasMany( m => m.Episodes );

            builder.Entity<Episode>()
                .HasKey( m => m.Id);

            base.OnModelCreating( builder );
        }
    }

}
