﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VideoStream.Components;

namespace VideoStream.Controllers {
    [Route( "api/[controller]" )]
    [ApiController]
    public class TheTVDBApiController :ControllerBase {
        private readonly TheTVDBApi _theTVDB;

        public TheTVDBApiController() {
            _theTVDB = new TheTVDBApi();
        }

        // GET: api/TheTVDBApi
        [HttpGet( "Images/{id}" )]
        public async Task<IActionResult> GetImagesAsync(int id) {
            await _theTVDB.Auth();
            var resp = await _theTVDB.GetImagesAsync( id );
            return Ok( resp );
        }

        [HttpGet( "SerieInfo/{id}" )]
        public async Task<IActionResult> GetSerieInfo(int id) {
            await _theTVDB.Auth();
            var resp = await _theTVDB.GetSerie( id );
            return Ok( resp );
        }

        [HttpGet( "EpisodesInfo/{id}/{page}" )]
        public async Task<IActionResult> GetEpisodesInfo(int id, int? page) {
            await _theTVDB.Auth();
            var resp = await _theTVDB.GetSeasons( id, page ?? 1 );
            return Ok( resp );
        }

        [HttpGet( "SeasonsInfo/{id}" )]
        public async Task<IActionResult> GetSeasonsInfo(int id) {
            await _theTVDB.Auth();
            var resp = await _theTVDB.GetSeasonsSummary( id );
            return Ok( resp );
        }

        [HttpGet( "SearchSerie/{seriename}" )]
        public async Task<IActionResult> GetSearchSerieAsync(string serieName) {
            await _theTVDB.Auth();
            var resp = await _theTVDB.SearchAsync( serieName );
            return Ok( resp );
        }
    }
}
