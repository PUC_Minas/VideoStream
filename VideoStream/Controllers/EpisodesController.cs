﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VideoStream.Components;
using VideoStream.Models;

namespace VideoStream.Controllers {
    [Route( "api/[controller]" )]
    [ApiController]
    public class EpisodesController :ControllerBase {
        private readonly DatabaseContext _context;
        private readonly TheTVDBApi _theTVDB;

        public EpisodesController(DatabaseContext context) {
            _context = context;
            _theTVDB = new TheTVDBApi();
        }

        // GET: api/Episodes
        [HttpGet( "GetEpisodesInSeason/{seasonId}" )]
        public IEnumerable<Episode> GetEpisodes([FromRoute] int seasonId) {
            if (_context.Episodes.Count() > 0) {
                return _context.Episodes.Where( m => m.SeasonId == seasonId ).OrderBy( a => a.Number );
            }
            return new List<Episode>();
        }

        // GET: api/Episodes/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetEpisode([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var episode = await _context.Episodes.FindAsync( id );

            if (episode == null) {
                return NotFound();
            }

            return Ok( episode );
        }

        // PUT: api/Episodes/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutEpisode([FromRoute] int id, [FromBody] Episode episode) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != episode.Id) {
                return BadRequest();
            }

            _context.Entry( episode ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!EpisodeExists( id )) {
                    return NotFound();
                } else {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Episodes
        [HttpPost]
        public async Task<IActionResult> PostEpisode([FromBody] Episode episode) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            await _theTVDB.Auth();
            var season = _context.Seasons.SingleOrDefault( m => m.Id == episode.SeasonId );
            var serie = _context.Series.SingleOrDefault( m => m.Id == season.SerieId );

            var episodes = await _theTVDB.GetSeasons( serie.TheTVDBId, season.Number );
            var data = episodes.SingleOrDefault( a => a.AiredEpisodeNumber == episode.Number );
            if (data == null) {
                return BadRequest( "Essa episodio não existe nessa temporada" );
            }

            if( String.IsNullOrEmpty( episode.File)) {
                string commonPath = @"X:\Séries\" + serie.Name + @"\" + season.Number + @"ª Temporada\";
                //                string file = serie.Name.Substring(0, serie.Name.Length-7) + " - "
                var files = Directory
                    .EnumerateFiles( commonPath )
                    .Where( file => file.ToLower().EndsWith( "mkv" ) || file.ToLower().EndsWith( "avi" ) || file.ToLower().EndsWith( "mp4" ) )
                    .ToList();
                var choosed = files[ episode.Number - 1 ];
                episode.File = Path.Combine( commonPath, choosed );
            } else if (!System.IO.File.Exists( episode.File )) {
                return BadRequest( "Esse arquivo não existe no servidor" );
            }

            var image = await _theTVDB.GetEpisodeImageAsync( data.Id );

            episode.Title = data.EpisodeName;
            episode.Description = data.Overview;
            episode.Poster = image.Filename;

            _context.Episodes.Add( episode );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetEpisode", new { id = episode.Id }, episode );
        }

        // DELETE: api/Episodes/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteEpisode([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var episode = await _context.Episodes.FindAsync( id );
            if (episode == null) {
                return NotFound();
            }

            _context.Episodes.Remove( episode );
            await _context.SaveChangesAsync();

            return Ok( episode );
        }

        private bool EpisodeExists(int id) {
            return _context.Episodes.Any( e => e.Id == id );
        }

        [HttpGet( "Stream/{id}" )]
        public async Task<IActionResult> Stream([FromRoute] int id) {

            var episode = await _context.Episodes.FindAsync( id );
            if (System.IO.File.Exists( episode.File )) {
                FileStream fs = new FileStream( episode.File, FileMode.Open );
                return new FileStreamResult( fs, new MediaTypeHeaderValue( "video/mp4" ).MediaType );
            } else {
                return NotFound( "Episode not exists!" );
            }

        }

        [HttpGet( "Subtitle/{id}" )]
        public async Task<IActionResult> Subtitle([FromRoute] int id) {

            var episode = await _context.Episodes.FindAsync( id );
            FileStream fs = null;
            string tempPath = System.IO.Directory.GetCurrentDirectory() + @"/tmp/";
            string subtitleName = Path.GetFileNameWithoutExtension( episode.File ) + ".vvt";
            if (System.IO.File.Exists( Path.Combine( tempPath, subtitleName ) )) {
                fs = new FileStream( Path.Combine( tempPath, subtitleName ), FileMode.Open );
            } else if (System.IO.File.Exists( Path.ChangeExtension( episode.File, ".srt" ) )) {
                try {
                    SubtitleConverter.ConvertSRTToVTT( Path.ChangeExtension( episode.File, ".srt" ), 0, true, Path.Combine( tempPath, subtitleName ) );
                    fs = new FileStream( Path.Combine( tempPath, subtitleName ), FileMode.Open );
                } catch (Exception e) {
                    return BadRequest( "name: Error on convert to vvt, error:" + e.Message );
                }
            } else {
                return NotFound( "Subtitle not exists!" );
            }
            return new FileStreamResult( fs, new MediaTypeHeaderValue( "text/vtt" ).MediaType );

        }

    }
}