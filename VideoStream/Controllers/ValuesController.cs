﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace VideoStream.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        //// GET api/values
        //[HttpGet]
        //public ActionResult<IEnumerable<string>> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        [HttpGet]
        public IActionResult GetVideoContent() {

            //if (System.IO.File.Open( "BigBuckBunny.mp4", FileMode.Open, out FileStream fs )) {
            var file = @"C:\Users\Administrador.WIN-JPQQV33KEPG\Downloads\Torrent\Séries\One Piece\OpEx_844_HD.mkv";
            if (System.IO.File.Exists(file)){ 
                FileStream fs = new FileStream( file, FileMode.Open );
                Console.WriteLine( "TESTE" );
                return new FileStreamResult( fs, new MediaTypeHeaderValue( "video/mp4" ).MediaType );
            }

            return BadRequest();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
