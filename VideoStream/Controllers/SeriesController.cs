﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VideoStream.Components;
using VideoStream.Models;

namespace VideoStream.Controllers {
    [Route( "api/[controller]" )]
    [ApiController]
    public class SeriesController :ControllerBase {
        private readonly DatabaseContext _context;
        private readonly TheTVDBApi _theTVDB;

        public SeriesController(DatabaseContext context) {
            _context = context;
            _theTVDB = new TheTVDBApi();
        }

        // GET: api/Series
        [HttpGet]
        public IEnumerable<Serie> GetSeries() {
            List<Serie> resp = new List<Serie>();
            if (_context.Series.Count() > 0) {
                return _context.Series.Include( m => m.Seasons ).OrderBy( a => a.Name );
            }
            return resp;
        }

        // GET: api/Series/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetSerie([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var serie = await _context.Series.FindAsync( id );

            if (serie == null) {
                return NotFound();
            }

            return Ok( serie );
        }

        // PUT: api/Series/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutSerie([FromRoute] int id, [FromBody] Serie serie) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != serie.Id) {
                return BadRequest();
            }

            _context.Entry( serie ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!SerieExists( id )) {
                    return NotFound();
                } else {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Series
        [HttpPost]
        public async Task<IActionResult> PostSerie([FromBody] Serie serie) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            await _theTVDB.Auth();
            var search = await _theTVDB.SearchAsync( serie.Name );
            var data = search.OrderByDescending( a => a.FirstAired ).First();
            var image = await _theTVDB.GetImagesAsync( data.Id );
            if (data.SeriesName.Equals( "The Flash (2014)" )) {
                serie.Name = data.SeriesName;
            } else {
                serie.Name = data.SeriesName + " (" + data.FirstAired.Substring( 0, 4 ) + ")";
            }
            serie.Description = data.Overview;
            serie.TheTVDBId = data.Id;
            serie.Poster = image.Last();

            _context.Series.Add( serie );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetSerie", new { id = serie.Id }, serie );
        }

        // DELETE: api/Series/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteSerie([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var serie = await _context.Series.FindAsync( id );
            if (serie == null) {
                return NotFound();
            }

            _context.Series.Remove( serie );
            await _context.SaveChangesAsync();

            return Ok( serie );
        }

        private bool SerieExists(int id) {
            return _context.Series.Any( e => e.Id == id );
        }
    }
}