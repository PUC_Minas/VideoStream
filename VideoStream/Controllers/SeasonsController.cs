﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VideoStream.Components;
using VideoStream.Models;

namespace VideoStream.Controllers {
    [Route( "api/[controller]" )]
    [ApiController]
    public class SeasonsController :ControllerBase {
        private readonly DatabaseContext _context;
        private readonly TheTVDBApi _theTVDB;

        public SeasonsController(DatabaseContext context) {
            _context = context;
            _theTVDB = new TheTVDBApi();
        }

        // GET: api/Seasons
        [HttpGet( "GetSeasonsInSerie/{serieId}" )]
        public IEnumerable<Season> GetSeasons([FromRoute] int serieId) {
            if (_context.Seasons.Count() > 0) {
                return _context.Seasons.Include( m => m.Episodes ).Where( m => m.SerieId == serieId ).OrderBy( a => a.Number );
            }
            return new List<Season>();
        }

        // GET: api/Seasons/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetSeason([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var season = await _context.Seasons.FindAsync( id );

            if (season == null) {
                return NotFound();
            }

            return Ok( season );
        }

        // PUT: api/Seasons/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutSeason([FromRoute] int id, [FromBody] Season season) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != season.Id) {
                return BadRequest();
            }

            _context.Entry( season ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!SeasonExists( id )) {
                    return NotFound();
                } else {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Seasons
        [HttpPost]
        public async Task<IActionResult> PostSeason([FromBody] Season season) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            await _theTVDB.Auth();
            var serie = _context.Series.SingleOrDefault( m => m.Id == season.SerieId );
            var seasonSummary = await _theTVDB.GetSeasonsSummary( serie.TheTVDBId );
            if(seasonSummary.AiredSeasons.SingleOrDefault( a => Convert.ToInt32(a) == season.Number) == null) {
                return BadRequest( "Essa temporada não existe nessa serie" );
            }

            var image = await _theTVDB.GetImagesAsync( serie.TheTVDBId );
            season.Poster = image[season.Number];

            _context.Seasons.Add( season );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetSeason", new { id = season.Id }, season );
        }

        // DELETE: api/Seasons/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteSeason([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var season = await _context.Seasons.FindAsync( id );
            if (season == null) {
                return NotFound();
            }

            _context.Seasons.Remove( season );
            await _context.SaveChangesAsync();

            return Ok( season );
        }

        private bool SeasonExists(int id) {
            return _context.Seasons.Any( e => e.Id == id );
        }
    }
}